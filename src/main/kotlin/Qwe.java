import kotlin.Unit;
import ompo.builders.DocumentBuilder;
import ompo.model.cart.Cart;
import ompo.model.cart.CartBuilder;
import ompo.model.cart.UrlsServiceInfo;
import ompo.model.typicalcfg.TypicalConfigurationController;
import ompo.model.typicalcfg.TypicalConfigurationException;
import ompo.model.typicalcfg.TypicalPreConfiguration;
import ompo.network.dto.requests.DTOAuthorization;
import ompo.network.dto.requests.DTOGetOfferingBodyRequest;
import ompo.network.dto.requests.DTOGetOfferingMethodParams;
import ompo.network.dto.requests.DTOMatchProperty;
import ompo.network.dto.requests.DTOMatchPropertySpecificationCondition;
import ompo.network.dto.requests.DTOMatchPropertySpecificationConditionString;
import ompo.network.dto.requests.DTOMatchPropertyString;
import ompo.network.dto.requests.DTOPocv2BodyRequest;
import ompo.network.dto.requests.DTOPocv2MethodParams;
import ompo.network.request.AddressMPZ;
import ompo.network.request.AddressOrpon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Qwe {
    public static void main(String[] args) throws InterruptedException {

        var LITE_API = "https://apigw.south.rt.ru/apiman-gateway/epc-inet/srv-webcom-net-stage/1.8.4";
        var EPC_API = "https://apigw.south.rt.ru/apiman-gateway/epc-inet/srv-webcom-net-stage/1.8.4";
        var DISPATCHER_URL = "";
        var APIKEY = "65e24289-697b-4be2-9396-518087cd6024";
        var token = new DTOAuthorization("EISSD_TEST", "IGAF5G2GMR12X514GYXRZZK1RZA81IY4YCVYWL8OVZLWFFVP1JD0UEWAGKN6Z0LL"); // требуется, если запросы находятся вне сети РТК.
        var urlsServiceInfo = new UrlsServiceInfo(LITE_API, EPC_API, DISPATCHER_URL, APIKEY, token);

        var cartBuilder = new CartBuilder(urlsServiceInfo);
        cartBuilder.exec().always(
                cart ->
                {
                    var methodParams = new DTOGetOfferingMethodParams();
                    //параметры из епк
                    methodParams.setId(416308018788L);
                    methodParams.setVersion("1.0");
                    methodParams.addDistributionChannel(new String[]{"62"});
                    methodParams.setAddress(new AddressMPZ(2492396L));

                    List<DTOMatchPropertySpecificationCondition> fixedProperties = new ArrayList<>();
                    fixedProperties.add(
                            new DTOMatchPropertySpecificationConditionString(
                                    "pR7",
                                    new String[]{"POSTPAIDUNLIM"},
                                    null,
                                    true));
//                    methodParams.addFixedProperties(fixedProperties.toArray(new DTOMatchPropertySpecificationCondition[0]));
                    DTOMatchProperty property = new DTOMatchPropertyString(
                            "FRONT_AVAILABILITY",
                            new String[]{"WEB_COMPONENT"},
                            true);
                    methodParams.addProperty(property);

                    var bodyRequest = new DTOGetOfferingBodyRequest();
                    bodyRequest.setMethodParams(methodParams);
                    var task = cart.addProductOffer(bodyRequest);
                    task.exec().always(doc ->
                            {
                                var controller = new TypicalConfigurationController(doc);
                                Iterator<TypicalPreConfiguration> configurationIterator = controller.getTypicalConfigurations();
                                configurationIterator.forEachRemaining(config -> {
                                    try {
                                        config.apply();
                                    } catch (TypicalConfigurationException e) {
                                        e.printStackTrace();
                                    }
                                });

                                var params = new DTOPocv2MethodParams();
                                var body = new DTOPocv2BodyRequest();
                                body.setMethodParams(params);

                                cart.exportPOCv2(body).exec().always(
                                        pocv2 ->
                                        {
                                            return Unit.INSTANCE;
                                        }, err ->
                                        {
                                            err.printStackTrace();
                                            return Unit.INSTANCE;
                                        }
                                );
                                return Unit.INSTANCE;
                            },
                            err ->
                            {
                                err.printStackTrace();
                                return Unit.INSTANCE;
                            }
                    );
                    return Unit.INSTANCE;
                },
                err ->
                {
                    err.printStackTrace();
                    return Unit.INSTANCE;
                }
        );

        Thread.sleep(50000);
    }

}
